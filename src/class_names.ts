const ClassNames = {
    success: "bg-success",
    error: "bg-danger",
    progress_bar: "progress-bar",
    page_progress_bar: "page-progress-bar",
}
///Export
export { ClassNames }