import { ClassNames } from "./class_names"

class ProgressBar {
    protected timeAwake: number = 10
    protected defaultTimeLoad = 750 //ms
    //Timer
    protected timer: any = null
    protected state: any = {
        averageLoadTime: this.defaultTimeLoad,
        error: false,
        reset: false,
        percent: 0
    }
    //Time start
    protected timeStart: number = 0
    /**
     * Constructor
     */
    constructor({ timeLoad }: any = {}) {
        //Time load
        if (timeLoad) {
            this.defaultTimeLoad = timeLoad
        }
        //Add bar
        this.addBar()
    }
    addBar = (): HTMLElement | null => {
        try {
            const $body = document.getElementsByTagName("body")[0]
            const $progressBar = document.createElement("div")
            $progressBar.classList.add(
                ClassNames.page_progress_bar,
                "progress-bar",
                "progress-bar-striped",
                "progress-bar-animated",
                ClassNames.success,
            )
            $body.appendChild($progressBar)
            return $progressBar
        } catch (error) { }
        //Null
        return null
    }
    /**
     * Start progress bar
     */
    startLoadPage = () => {
        let { percent } = this.state
        const { averageLoadTime } = this.state
        //Time start
        this.timeStart = Date.now()
        //Timer
        this.timer = setInterval(() => {
            percent += this.timeAwake / averageLoadTime * 100
            if (percent > 100) {
                percent = 0
            }
            this.setState({ percent })
        }, this.timeAwake)
    }
    /**
     * Show bar
     */
    getBar = (): HTMLElement | null => {
        let $progressBar = document.getElementsByClassName(ClassNames.page_progress_bar)[0]
        if (!$progressBar) {
            return this.addBar()
        }
        //Return
        return $progressBar as HTMLElement
    }
    protected updateBar = () => {
        const { percent, error } = this.state
        const $progressBar = this.getBar()
        if (!$progressBar) {
            return
        }
        //Reset
        if (percent === 0) {
            $progressBar.classList.remove(ClassNames.error)
            $progressBar.classList.add(ClassNames.success)
        }

        //Percent
        $progressBar.style.width = percent + "%"
        //Error
        if (error) {
            $progressBar.classList.remove(ClassNames.success)
            $progressBar.classList.add(ClassNames.error)
        }
    }
    /**
     * End progress bar
     */
    endLoadPage = () => {
        //Remove timer
        if (this.timer) {
            clearInterval(this.timer)
            this.timer = null
        }
        //Update percent
        this.setState({
            percent: 0
        })
    }
    /**
     * Set page be error
     */
    setErrorPage = () => {
        this.errorLoadPage()
    }
    errorLoadPage = () => {
        //Remove timer
        if (this.timer) {
            clearInterval(this.timer)
            this.timer = null
        }
        //Update percent
        this.setState({
            percent: 100,
            error: true
        })
    }
    /**
     * Update
     */
    protected update = () => {
        //Update bar
        this.updateBar()
    }
    /**
     * Set state
     */
    protected setState = (newState: any) => {
        this.state = {
            ...this.state,
            ...newState
        }
        //Update
        this.update()
    }
}
//Export
export default ProgressBar 