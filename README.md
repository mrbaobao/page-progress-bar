# page-progress-bar

Progress bar for web app: Nextjs, Reactjs, Vuejs, ...

# Install:
* yarn add page-progress-bar
* npm i page-progress-bar

# Usage:
1. New ProgressBar
```
const progressBar = new ProgressBar({timeLoad: 250}) //timeLoad: number --> milliseconds

//OR
const progressBar = new ProgressBar() //timeLoad default = 750ms

```

2. Start loading page
```
//When page start loading
progressBar.startLoadPage()
```

3. End loading page
```
progressBar.endLoadPage()
```
4. Set Page Error
```
progressBar.setErrorPage()
```
5. CSS
```
.page-progress-bar {
    position: fixed;
    z-index: 9999999;
    left: 0;
    top: 0;
    height: 2px;
}
Bootstrap class:
    "progress-bar",
    "progress-bar-striped",
    "progress-bar-animated", 
    "bg-success",
    "bg-danger
```
