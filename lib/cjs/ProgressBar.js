"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const class_names_1 = require("./class_names");
class ProgressBar {
    /**
     * Constructor
     */
    constructor({ timeLoad } = {}) {
        this.timeAwake = 10;
        this.defaultTimeLoad = 750; //ms
        //Timer
        this.timer = null;
        this.state = {
            averageLoadTime: this.defaultTimeLoad,
            error: false,
            reset: false,
            percent: 0
        };
        //Time start
        this.timeStart = 0;
        this.addBar = () => {
            try {
                const $body = document.getElementsByTagName("body")[0];
                const $progressBar = document.createElement("div");
                $progressBar.classList.add(class_names_1.ClassNames.page_progress_bar, "progress-bar", "progress-bar-striped", "progress-bar-animated", class_names_1.ClassNames.success);
                $body.appendChild($progressBar);
                return $progressBar;
            }
            catch (error) { }
            //Null
            return null;
        };
        /**
         * Start progress bar
         */
        this.startLoadPage = () => {
            let { percent } = this.state;
            const { averageLoadTime } = this.state;
            //Time start
            this.timeStart = Date.now();
            //Timer
            this.timer = setInterval(() => {
                percent += this.timeAwake / averageLoadTime * 100;
                if (percent > 100) {
                    percent = 0;
                }
                this.setState({ percent });
            }, this.timeAwake);
        };
        /**
         * Show bar
         */
        this.getBar = () => {
            let $progressBar = document.getElementsByClassName(class_names_1.ClassNames.page_progress_bar)[0];
            if (!$progressBar) {
                return this.addBar();
            }
            //Return
            return $progressBar;
        };
        this.updateBar = () => {
            const { percent, error } = this.state;
            const $progressBar = this.getBar();
            if (!$progressBar) {
                return;
            }
            //Reset
            if (percent === 0) {
                $progressBar.classList.remove(class_names_1.ClassNames.error);
                $progressBar.classList.add(class_names_1.ClassNames.success);
            }
            //Percent
            $progressBar.style.width = percent + "%";
            //Error
            if (error) {
                $progressBar.classList.remove(class_names_1.ClassNames.success);
                $progressBar.classList.add(class_names_1.ClassNames.error);
            }
        };
        /**
         * End progress bar
         */
        this.endLoadPage = () => {
            //Remove timer
            if (this.timer) {
                clearInterval(this.timer);
                this.timer = null;
            }
            //Update percent
            this.setState({
                percent: 0
            });
        };
        /**
         * Set page be error
         */
        this.setErrorPage = () => {
            this.errorLoadPage();
        };
        this.errorLoadPage = () => {
            //Remove timer
            if (this.timer) {
                clearInterval(this.timer);
                this.timer = null;
            }
            //Update percent
            this.setState({
                percent: 100,
                error: true
            });
        };
        /**
         * Update
         */
        this.update = () => {
            //Update bar
            this.updateBar();
        };
        /**
         * Set state
         */
        this.setState = (newState) => {
            this.state = Object.assign(Object.assign({}, this.state), newState);
            //Update
            this.update();
        };
        //Time load
        if (timeLoad) {
            this.defaultTimeLoad = timeLoad;
        }
        //Add bar
        this.addBar();
    }
}
//Export
exports.default = ProgressBar;
