declare class ProgressBar {
    protected timeAwake: number;
    protected defaultTimeLoad: number;
    protected timer: any;
    protected state: any;
    protected timeStart: number;
    /**
     * Constructor
     */
    constructor({ timeLoad }?: any);
    addBar: () => HTMLElement | null;
    /**
     * Start progress bar
     */
    startLoadPage: () => void;
    /**
     * Show bar
     */
    getBar: () => HTMLElement | null;
    protected updateBar: () => void;
    /**
     * End progress bar
     */
    endLoadPage: () => void;
    /**
     * Set page be error
     */
    setErrorPage: () => void;
    errorLoadPage: () => void;
    /**
     * Update
     */
    protected update: () => void;
    /**
     * Set state
     */
    protected setState: (newState: any) => void;
}
export default ProgressBar;
