"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ClassNames = void 0;
const ClassNames = {
    success: "bg-success",
    error: "bg-danger",
    progress_bar: "progress-bar",
    page_progress_bar: "page-progress-bar",
};
exports.ClassNames = ClassNames;
