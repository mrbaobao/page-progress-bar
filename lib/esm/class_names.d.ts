declare const ClassNames: {
    success: string;
    error: string;
    progress_bar: string;
    page_progress_bar: string;
};
export { ClassNames };
